import mysql.connector


mydb = mysql.connector.connect(
  host="35.200.185.193",
  user="root",
  passwd="alok3796"
)
db_name = "crawler_db"
table_name = "video_links"
mycursor = mydb.cursor()

########################### CREATE TABLE QUERY ######################################
def create_table():
    mycursor.execute("CREATE TABLE "+table_name+" ( id INT NOT NULL AUTO_INCREMENT, source text, channel text, video_title text, video_url text, download_flag INT, views text, video_age text, time_stamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(id) )")
    print("Table Created")

try:
    mycursor.execute("USE "+ db_name)
    print("Database Present, Using Database "+db_name)
except Exception as e :
    if e.errno == 1049:
        print(str(e)+" Database Not present")
        mycursor.execute("CREATE DATABASE "+db_name)
        mycursor.execute("USE "+db_name)
        create_table()
    else:
        print("Some Error, Check Code")

def select_query(query):
    try:
        mycursor.execute(query)
    except Exception as e :
        if e.errno == 1146 :
            print(e)
            create_table()
            mycursor.execute(query)
        else:
            print(e)

def insert_query(query, value):
    try:
        mycursor.execute(query, value)
    except Exception as e :
        if e.errno == 1146 :
            print(e)
            create_table()
            mycursor.execute(query, value)
        else:
            print(e)

    mydb.commit()
    print(mycursor.rowcount, " Records inserted")


def update_query(query):
    try:
        mycursor.execute(query)
    except Exception as e :
        if e.errno == 1146 :
            print(e)
            create_table()
            mycursor.execute(query)
        else:
            print(e)

    mydb.commit()
    print(mycursor.rowcount, " Records updated")
