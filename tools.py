import re

def process_string( str ):
    str = re.sub('[\'\"&;]', '_', str)
    return str
