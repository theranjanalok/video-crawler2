#! /bin/bash
DEPENDENCIES="Installing Dependencies"
PIP3="Installing pip for Python"
SELENIUM="Installing Selenium for Python"
PVD="Installing Pyvirtualdisplay"
PYTUBE="Installing Pytube"
FIREFOX="Installing Firefox"
GECKO="Installing Geckodriver 0.24"
XVFB="Installing XVFB"
FFMPEG="Installing FFMPEG"
GITHUB="Fetching Crawler from Git Repository"
UPDATE="Update"
MYSQL="Installing MySQL"

echo $DEPENDENCIES
echo "====================================================================="
echo $UPDATE
sudo apt-get update

echo "====================================================================="
echo $PIP3
sudo apt install python3-pip

echo "====================================================================="
echo $FIREFOX
sudo apt-get install firefox

echo "====================================================================="
echo $PVD
sudo pip3 install pyvirtualdisplay

echo "====================================================================="
echo $GECKO
mkdir tools
cd tools
wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
tar -xvzf geckodriver*
chmod +x geckodriver
geckopath=$(pwd)
cd ..

echo "====================================================================="
echo $SELENIUM
sudo pip3 install selenium

echo "====================================================================="
echo $XVFB
sudo apt-get install xvfb

echo "====================================================================="
echo $FFMPEG
sudo apt install ffmpeg

echo "====================================================================="
echo $PYTUBE
sudo pip3 install pytube

echo "====================================================================="
echo $PYTUBE
sudo pip3 install mysql-connector

echo "====================================================================="

chmod +x crawler_run.sh
echo "RUN THE FOLLOWING COMMANDS"
echo "export PATH=\$PATH:$geckopath/"
echo "export PATH=\$PATH:/snap/bin/"
