from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import time
import csv
import sys
# import download_video
from os import path
import logging
import datetime
import log_generate
import db
import tools

#
display = Display(visible=0, size=(1366, 768))
display.start()

driver = webdriver.Firefox()
driver.set_window_size(1366, 768)

channel_link = sys.argv[1]

# driver.get("https://www.youtube.com/user/GameofThrones/videos")
driver.get(channel_link)
# channel_name = str(driver.find_elements_by_tag_name('title'))
channel_name = str(driver.title)
print("Channel Name = " +channel_name)

SCROLL_PAUSE_TIME = 1.0
# Get scroll height
js = "return Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight );"

js2 = "window.scrollTo(0, Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight ) );"

last_height = driver.execute_script(js)
# print("last height = "+ str(last_height))

def checkspin():
    try :
        spinners = driver.find_elements_by_tag_name('paper-spinner')
    except StaleElementReferenceException:
        print("Error in finding elements")
        logging.info(str(time.asctime( time.localtime(time.time()) )) + "Error in finding elements")
    if ( len(spinners) > 0 ):
        try:
            # spinId = str(spinners[0].get_attribute('id'))
            # spinClass = str(spinners[0].get_attribute('class'))
            spinHidden = str(spinners[0].get_attribute('aria-hidden'))
        except:
            print("Error in finding the aria-hidden")
            logging.info(str(time.asctime( time.localtime(time.time()) )) + "Error in finding the aria-hidden")
            spinHidden='Active'
            driver.refresh()
            time.sleep(2)
        # spinActive = spinners[0].get_attribute('active')
        if ( spinHidden == 'true' ):
            spin = "Hidden"
        else :
            spin = "Active"
    else :
        spin = "No Spinner Tag Present"
    return spin

while True:
    spinner = str(checkspin()).strip()
    if spinner == 'Hidden':
        # print("Scrolling Down")
        driver.execute_script(js2)
        time.sleep(SCROLL_PAUSE_TIME)
    # elif spinner == 'Active' :
        # print("Loading Content")
    elif spinner == 'No Spinner Tag Present' :
        print("No more videos")
        break
    newh = driver.execute_script(js)
    # print ("New Height is = "+ str(newh))

time.sleep(1)
# assert "GameofThrones" in driver.title
# print(driver)
print("Page loaded")
logging.info(str(time.asctime( time.localtime(time.time()) )) + "Page Loaded")
elems = driver.find_elements_by_class_name("style-scope ytd-grid-video-renderer")

insert_query = "INSERT INTO video_links(source, channel, video_title, video_url, download_flag, views, video_age) VALUES ( %s, %s, %s, %s, %s, %s, %s )"

for i in range(len(elems)):
    try:
        links = elems[i].find_elements_by_tag_name('a')
        vlink = links[0].get_attribute('href')
        title = links[1].get_attribute('title')
        a = elems[i].find_element_by_id('metadata-line')
        b = a.find_elements_by_tag_name('span')
        views = b[0].get_attribute('innerHTML')
        vtime = b[1].get_attribute('innerHTML')
        localtime = time.asctime( time.localtime(time.time()) )
        logging.info(str(time.asctime( time.localtime(time.time()) )) + "Video Link Found = "+vlink+" "+ channel_name)
        values = ("Youtube", tools.process_string(channel_name), tools.process_string(title), vlink, 0, views, str(vtime))
        try:
            db.insert_query(insert_query, values)
        except Exception as e:
            print(e+" Error in Database Insert Query")
            logging.info(str(time.asctime( time.localtime(time.time()) )) + "Video Link could not be inserted "+vlink+" "+ channel_name)

    except Exception as e:
        logging.error(str(time.asctime( time.localtime(time.time()) )) + str(e))
        print(str(e))


time.sleep(1)
# print(len(vlinks))
driver.close()
